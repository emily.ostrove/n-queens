from pprint import pprint

def is_in_danger(board, row_index, column_index):
    # check the row for a queen
    for index, value in enumerate(board[row_index]):
        # print("is in danger: ", index, value)
        # print(index, column_index)
        if value == 0 or index == column_index:
            pass
        else:
            return True
    
    # check the col for a queen
    # for index, row in enumerate(board):
    #     if row[column_index] == 0 or row_index:
    #         pass
    #     else:
    #         return True
    return False

def place_queen_in_column(board, n, column_index):
    # base case
    if column_index >= n:
        return
    for row_number in range(n):
        board[row_number][column_index]=1

        #print(is_in_danger(board, row_number, column_index))
        if is_in_danger(board, row_number, column_index):
            # if in danger remove queen
            board[row_number][column_index]=0
        else:
            place_queen_in_column(board, n, column_index + 1)

def n_queens(n):
    row = []
    board = []
    for i in range(n):
        row.append(0)
    for i in range(n):
        board.append(row)

    place_queen_in_column(board, n, 0)

    return board

# print(n_queens(4))

if __name__ == "__main__":
    board = n_queens(4)
    pprint(board, width=100)
